# Self-Paced Labs

Self-Paced Labs is a service that provides users a catalogue of short tutorials where users will be able to download and execute these tutorials on their own laptop or using the BDTI resources.

Self-Paced Labs are short tutorials developed using Jupyter Notebooks for an interactive learning experience. Users can read the text explanation and directly run the relevant Python code snippet. These short tutorials introduce BDTI users to popular Python Frameworks for Data Science use cases, and use open-source data from [EU Open Data Portal](https://data.europa.eu/en) and [Kaggle](https://www.kaggle.com/datasets) (an open-source machine learning community) to demonstrate these Python Frameworks.

For more information, please visit [the BDTI Resources page](https://ec-europa.github.io/bdti-infrastructure/Resources/).

---
To test these Jupyter Notebooks, you can use the interactive notebooks by clicking on the below Binder link:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.fpfis.eu%2Fpublic-datateam%2Fdataplatform-self-paced-labs/master)

Some notebooks require reference to Cloud resources. Therefore, only the below notebooks are runnable using the above Binder link:

-	01 – General: Data Exploration
-	03 – Machine Learning: Regression
-	03 – Machine Learning: Classification
-	03 – Machine Learning: Clustering
-	03 – Machine Learning: Deep Learning
-	03 – Machine Learning: Time Series Analysis

---

Contributor: Wu, Ze Wen (https://github.com/zewenwu)

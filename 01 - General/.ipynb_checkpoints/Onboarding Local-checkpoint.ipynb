{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img align=\"right\" src=\"../img/DEP.png\" width=\"400px\">\n",
    "\n",
    "# Onboarding Local Computer (with Python)\n",
    "\n",
    "This tutorial focuses on connecting to BDTI Cloud resources programmatically using Jupyter Notbook and Python in the BDTI Data Science Studio. \n",
    "\n",
    "Using the user interface from different BDTI Cloud resources can be time consuming. Code snippets from this Notebooks can be used when you want to create a single Jupyter Notebook that connects to multiple BDTI resources programmatically.\n",
    "\n",
    "In this tutorial, we introduce the different use cases of connecting to the BDTI Cloud resources programmatically using your local computer.\n",
    "\n",
    "Contents:\n",
    "- Setting up Conda virtual environments in Jupyter Notebooks.\n",
    "- Setting up AWS credentials on the machine.\n",
    "- Downloading and uploading data in AWS S3 using Boto3.\n",
    "\n",
    "What you will learn:\n",
    "- Create and install Python packages in Conda virtual environments and import the environment in Jupyter Notebooks.\n",
    "- Configuring AWS credentials for Boto3 client.\n",
    "- Basic commands on how you can download data from and upload data to your BDTI AWS S3 bucket.\n",
    "\n",
    "Remark:\n",
    "- You have to run the code cells in sequence. Some code blocks may depend on variables from previous code blocks.\n",
    "\n",
    "Source:\n",
    "- [Anaconda Documentation](https://docs.anaconda.com/)\n",
    "- [Anaconda Cheat Sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)\n",
    "- [Boto3 Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conda virtual environments in Jupyter Notebooks\n",
    "\n",
    "When you start a new Python project, you will probably need to install several dependency packages that will be needed in your Python code. The best practice is to create a new Conda environment and install all the required Python packages into this Conda environment without interferring with the Conda environments of other users of the same project.\n",
    "\n",
    "Additionally, we provide instructions on how to list the Conda environment in Jupyter Notebooks.\n",
    "\n",
    "> **Remark**: All the code in this section should run in the Bash of your machine.\n",
    "\n",
    "### Viewing all the Conda environments\n",
    "\n",
    "You can view the existing Conda environments by using the **info** command of Conda:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda info --envs```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating a new Conda environment\n",
    "\n",
    "To create a new Conda environment to install all your dependency packages, you can use the **create** command of Conda. In the example below, we will create a new Conda environment named *my_env*, using Python 3.6. You can substitude the name to your liking."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda create --name my_env```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Activate a Conda environment\n",
    "\n",
    "Now that you have created a new Conda environment, you need to activate it so that all the future Python packages will be installed in this new Conda environment. To do this, you need to use the **activate** command of Conda."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda activate my_env```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Furthermore, you can check that your Conda environment has been activated using the **info** command. You can confirm by observing that the start (_*_) is next to your listed Conda environment. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda info --envs```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setup Conda environment on Jupyter Notebooks\n",
    "\n",
    "In the new Conda environment, you need to set it up in Jupyter Notebooks to be able to refer it when you work in Jupyter Notebooks.\n",
    "\n",
    "First, install ipykernel using the following command to be executed in the Bash."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda install -c anaconda ipykernel```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, list your Conda environment as one of the available Conda environments in Jupyter Notebook using the following command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python -m ipykernel install --user --name=my_env```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, you can create a new Jupyter Notebooks using the Conda environment you have just added. Please check this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installing packages in your Conda environment\n",
    "\n",
    "**All the packages that your Jupyter Notebooks depend on should be installed in the Bash of the machine.** This can be done using the **install** command. For a list of all the available packages in Anaconda, consult the [its Anaconda Documentation](https://docs.anaconda.com/anaconda/packages/pkg-docs/). For example you can install the *matplotlib* package this way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda install matplotlib```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Listing packages in your Conda environment\n",
    "\n",
    "To list all the packages in your current Conda environment, you can use the **list** command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda list```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing packages in you Conda environment\n",
    "\n",
    "You could remove a list of package from a specific Conda environment. For example, you could remove the packages *matplotlib* and *scikit-learn* from the Conda environment *my_env* using the **remove** command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda remove --name my_env matplotlib scikit-learn```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deleting a Conda environment\n",
    "\n",
    "You can delete a Conda environment and all the packages in it using the **env remove** command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda env remove --name my_env```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have\n",
    "\n",
    "Other use cases of Conda are out of scope in this tutorial. \n",
    "For more info regarding Conda environments and Conda commands, please consult:\n",
    "- [Anaconda Documentation](https://docs.anaconda.com/)\n",
    "- [Anaconda Cheat Sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up AWS credentials on the machine.\n",
    "\n",
    "You can use the AWS SDK for Python (Boto3) to create, configure, and manage AWS services, such as Amazon Elastic Compute Cloud (Amazon EC2) and Amazon Simple Storage Service (Amazon S3). The SDK provides an object-oriented API as well as low-level access to AWS services. For more info about Boto3, you can consult the [Boto3 Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html).\n",
    "\n",
    "> **Remark**: You will need to install Boto3 package in the terminal in your machine using conda install.\n",
    "\n",
    "If you **run several commands on this tutorial**, it might be that you require AWS credentials. This can be done by using the *init_aws_credentials.py* that was provided to you at the kick-off meeting. Please contact the technical team if this was not provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Username: zewwu@bdti.cef.eu\n",
      "········\n",
      "\n",
      "Response did not contain a valid SAML assertion\n"
     ]
    }
   ],
   "source": [
    "## Run the code script below to run the init_aws_credentials.py script to create an access key pair.\n",
    "%run init_aws_credentials.py\n",
    "\n",
    "## The script will prompt you to provide your BDTI credentials, like this:\n",
    "# Username: <your username>@bdti.cef.eu\n",
    "# Password: <your password>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we have to instruct the Boto3 SDK to use the SAML profile defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "import boto3\n",
    "\n",
    "boto3.setup_default_session(profile_name='saml')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations! Now you are ready to connect to the BDTI cloud resources using Boto3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Downloading and uploading data in AWS S3 using Boto3\n",
    "\n",
    "The first step as a BDTI Pilot member is to upload your local or online data to the BDTI AWS S3 bucket, which is the object storage solution in BDTI for storing and sharing data. This way, other BDTI resources such as your Data Science Studio and your WorkSpace can use your data in the S3 bucket.\n",
    "\n",
    "To access your AWS S3 bucket, you need to first install the latest version of Boto3 and configure it (see previous section).\n",
    "\n",
    "We start by specifying the AWS S3 bucket service you want to connect to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "import boto3\n",
    "\n",
    "# We want to connect to the S3 buckets.\n",
    "s3_resource = boto3.resource('s3')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the following codes allows you to upload a file to your S3 bucket and to download a file from your S3 bucket."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "File uploaded in S3!\n"
     ]
    }
   ],
   "source": [
    "# Defining bucket, filename to upload, and where location to upload\n",
    "bucket_name = \"cefbdti-miglierina-eu-west-1\"\n",
    "local_filename = '../example-data/color.json'\n",
    "s3_filename = 'example-data/color-uploaded.json'\n",
    "\n",
    "# Upload a new file\n",
    "data = open(local_filename, 'rb')\n",
    "s3_resource.Bucket(bucket_name).put_object(Key=s3_filename, Body=data)\n",
    "\n",
    "print(\"File uploaded in S3!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "File downloaded from S3!\n"
     ]
    }
   ],
   "source": [
    "# Defining bucket, filename to download from S3, and where location to download to locally\n",
    "bucket_name = \"cefbdti-miglierina-eu-west-1\"\n",
    "s3_filename = 'example-data/color-uploaded.json'\n",
    "local_filename = '../example-data/color-downloaded.json'\n",
    "\n",
    "# Download a file from S3\n",
    "s3_client = boto3.client('s3')\n",
    "s3_client.download_file(bucket_name, s3_filename, local_filename)\n",
    "\n",
    "print(\"File downloaded from S3!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "metadata": {
   "interpreter": {
    "hash": "63fd5069d213b44bf678585dea6b12cceca9941eaf7f819626cde1f2670de90d"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img align=\"right\" src=\"../img/DEP.png\" width=\"400px\">\n",
    "\n",
    "# Available Datasets from Open-Source Portals\n",
    "\n",
    "Contributor: Wu, Ze Wen (https://github.com/zewenwu)\n",
    "\n",
    "This tutorial focuses on additional guide for navigating popular open source data portals, such as:\n",
    "- data.europa.eu\n",
    "- eurostat\n",
    "- Kaggle\n",
    "\n",
    "Exploring different datasets as additional aid for your use case can be time consuming. Code snippets from this Notebooks can be used when you want to download a dataset from one of the above mentioned platforms programmatically.\n",
    "\n",
    "Contents:\n",
    "- Navigating data.europa.eu\n",
    "- Navigating eurostat\n",
    "- Navigating Kaggle\n",
    "- Setting up AWS credentials on the machine.\n",
    "- Downloading and uploading data in AWS S3 using Boto3\n",
    "\n",
    "What you will learn:\n",
    "- Introduction to navigating the open source data portals such as data.europa.eu, eurostat and Kaggle.\n",
    "- Establishing AWS credentials to allow dataset upload to S3 bucket.\n",
    "- Relevant code snippets to download from and upload to AWs S3 using Boto3\n",
    "\n",
    "Remark:\n",
    "- You have to run the code cells in sequence. Some code blocks may depend on variables from previous code blocks.\n",
    "- Please note that this might become outdated if one of the mentioned portals updates their pages. Please contact the technical team if one of the following sections does not work anymore because of this. \n",
    "\n",
    "Source:\n",
    "- [data.europa.eu](https://data.europa.eu/en)\n",
    "- [eurostat](https://ec.europa.eu/eurostat)\n",
    "- [Boto3 Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Navigating data.europa.eu\n",
    "\n",
    "**data.europa.eu** provides access to open data from international, EU, national, regional, local and geo data portals. It replaces the EU Open Data Portal and the European Data Portal. \n",
    "\n",
    "The portal addresses the whole data value chain, from data publishing to data reuse. Going beyond collecting metadata (data about data), the strategic objective of the portal is to improve *accessibility* and increase the *value* of open data. \n",
    "\n",
    "- **Accessibility.** How can users access this information? Where can they find it? How can it be made available in the first place? In domains, across domains, across countries? In what language? \n",
    "\n",
    "- **Value.** What is the purpose of the data and what are the economic, societal and/or democratic gains? What format will the data be published in? What is the critical mass?\n",
    "\n",
    "In this section we shall go through some of the sections of data.europa.eu: **Searching data** and **Using data**\n",
    "\n",
    "> **Remark.** The portal collects the metadata of public data made available across Europe. The data catalogues of origin may contain datasets with errors. The metadata contains the information made available with the dataset by the initial publisher. Different licences or absence of licences may occur, and re-users are invited to check with the owners/publishers of the data what terms and conditions apply to the reuse of the data. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Searching data\n",
    "\n",
    "On data.europa.eu, users can find datasets across categories from many different data portals. You can navigate to _Datasets_ and search for many available datasets available on data.europa.eu.\n",
    "\n",
    "In the _Data_ tab, you can navigate to _Datasets_ to view all the available datasets on data.europa.eu. For example, if you search fo _Health_ in the search bar and look for the use case _Health Oral Health_, you can find an application in Dental health sector:\n",
    "\n",
    "<img src=\"../img/available_datasets/oral_health.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Desciption:\n",
    "\n",
    "_Characterisation of the activity of the Oral Health Promotion Programme through the Dental Scheme, dental hygienist and referral for dental medicine in Primary Health Care. Identification of the number of dental cheques/referrals (HO and SOCSP) issued, used and treatments carried out according to the type of population covered by the PNPSO (children and young people attending public schools or IPSS [up to 18 years], pregnant women followed in the SNS, older beneficiaries of the solidarity supplement, with HIV/AIDS, users for early intervention in the oral cancer and users used in the SNS). * * Comments: * * In the year 2016 SNS users referenced for consultation in the functional units, have been covered by a pilot experiment, of which only users with Diabetes, neoplasms, cardiac or chronic respiratory illness, kidney failure in haemodialysis or peritoneal dialysis and transplantation have been included. From January 2017, the scope of intervention “Pilot Experience” became Oal Health in Primary Health Care, and all SNS users are covered by no longer clinical pathology criterion for referencing._"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you click on it, you can find several links to the available datasets in _csv_ and _json_ format:\n",
    "\n",
    "<img src=\"../img/available_datasets/download_manual.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, you could copy the link of the the _csv_ file (or any link) and download the dataset to a local folder programmatically, like this,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests\n",
    "\n",
    "# Download most recent data from EU Open Data Portal in a local file\n",
    "url = 'https://transparencia.sns.gov.pt/explore/dataset/saude-oral/download?format=csv&timezone=Europe/Berlin&use_labels_for_header=true'\n",
    "r = requests.get(url, allow_redirects=True)\n",
    "open('../eu-open-data/oral_health.csv', 'wb').write(r.content)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using data. \n",
    "\n",
    "This section provides details on how open data is being used, as well as its economic benefits, training and library. Here users will find eLearning modules about open data as well as training guides and a knowledge base referencing publications around open data. \n",
    "\n",
    "In the _Impact & Studies_ tab, you can navigate to _Use Cases_ to view the different use cases submitted on the portal. For example, if you search for _Covid_ in the search bar and look for the use case _United Kingdom - FinLine_, you can find an application in Economy & Finance sector:\n",
    "\n",
    "<img src=\"../img/available_datasets/use_cases.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Description:\n",
    "\n",
    "_The segment of small and medium enterprises (SMEs) has been severely hit by the COVID 19 pandemic. As Europe sets out for economic recovery, the survival of SMEs is essential. The FinLine app will help SMEs assess their financial viability and provide free customised advice for suitable grants and investment options. Also, it will provide a tool for building a community willing to share financial and technical advice as well as people’s skills during the post COVID 19 recovery._"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you click on it, you can find a poster demonstrating their use case:\n",
    "\n",
    "<img src=\"../img/available_datasets/geoFluxus.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Navigating eurostat\n",
    "\n",
    "[Eurostat](https://ec.europa.eu/eurostat/web/main/home) is the statistical office of the European Union. Their mission is to provide high quality statistics and data on Europe. \n",
    "\n",
    "Eurostat produces European statistics in partnership with National Statistical Institutes and other national authorities in the EU Member States. This partnership is known as the European Statistical System (ESS). It also includes the statistical authorities of the European Economic Area (EEA) countries and Switzerland.\n",
    "\n",
    "Eurostat coordinates statistical activities at Union level and more particularly inside the Commission.\n",
    "\n",
    "On the website, you can find:\n",
    "- The latest news on the platform\n",
    "- A database of the full range of data publically available at Eurostat. They are presented in multi-dimensional tables with various selection features and export formats.\n",
    "- A wide range of publications that present data for EU Member States, EU regions or G20 countries. The publication on Sustainable Development Goals is also included in this collection.\n",
    "\n",
    "In this section we shall go through some of the sections of eurostat: **Database** and **Publications**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Eurostat's database\n",
    "\n",
    "In the _Data_ tab, you can navigate to _Database_ to view all the available datasets on eurostat. \n",
    "\n",
    "<img src=\"../img/available_datasets/eurostat_database.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, if you navigate to _Tables by themes_, _Argiculture, forestry and fisheries_, _Agriculture_, _Agribultural production_, _livestock and meat_, you can find several datasets in the Food industry of EU states related to livestock and meat production. You can also download the dataset in _TSV_ format on this page:\n",
    "\n",
    "<img src=\"../img/available_datasets/eurostat_download.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Eurostat's publications\n",
    "\n",
    "In the _Publications_ tab, you can navigate to _All Publications_ to view all the available publications on eurostat. There are several types of publications available:\n",
    "- **Flagship publications** are overview publications that present data for EU Member States, EU regions or G20 countries. The publication on Sustainable Development Goals is also included in this collection.\n",
    "- **Interactive publications** contain short texts, interactive visualisations and maps, videos, photos, etc.\n",
    "- **Key figures** is a series of publications that presents key findings and gives an introduction to various statistical topics.\n",
    "\n",
    "<img src=\"../img/available_datasets/eurostat_publications.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, you can navigate to _Flagship publications_ and explore the publication _Sustainable development in the European Union — Monitoring report on progress towards the SDGs in an EU context — 2021 edition_:\n",
    "\n",
    "<img src=\"../img/available_datasets/eurostat_flagship_publications.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you click on it, you see that it is a report about the Sustainable development in EU.\n",
    "\n",
    "<img src=\"../img/available_datasets/eurostat_sustainability.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Description:\n",
    "\n",
    "Sustainable development is firmly anchored in the European Treaties and has been at the heart of European policy for a long time. The 2030 Agenda for Sustainable Development and its 17 Sustainable Development Goals (SDGs), adopted by the UN General Assembly in September 2015, have given a new impetus to global efforts for achieving sustainable development. The EU is fully committed to playing an active role in helping to maximise progress towards the Sustainable Development Goals. This publication is the fifth of Eurostat’s regular reports monitoring progress towards the SDGs in an EU context. The analysis in this publication builds on the EU SDG indicator set, developed in cooperation with a large number of stakeholders. The indicator set comprises 102 indicators and is structured along the 17 SDGs. For each SDG, it focuses on aspects that are relevant from an EU perspective. The monitoring report provides a statistical presentation of trends relating to the SDGs in the EU over the past five years (‘short-term’) and, when sufficient data are available, over the past 15 years (‘long-term’). The indicator trends are described on the basis of a set of specific quantitative rules. This 2021 edition also shows some of the early impacts of the COVID-19 pandemic that are visible in Eurostat’s official statistics.\n",
    "\n",
    "You can also download the report from eurostat."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Navigating Kaggle\n",
    "\n",
    "Kaggle is an online community of data scientists and machine learning practitioners. Kaggle allows users to find and publish data sets, explore and build models in a web-based data-science environment, work with other data scientists and machine learning engineers, and enter competitions to solve data science challenges.\n",
    "\n",
    "There are five tabs of Kaggle:\n",
    "- You can explore **competitions** of Kaggle to see whether some use cases are similar to your own use case, for getting inspirations on how to formulate the data problem.\n",
    "- The **datasets** of Kaggle are often ready-to-analyze, not requiring too much data preprocessing. We recommend to explore these datasets for educational purposes, i.e., to try out different data science techniques on datasets other than your own.\n",
    "- The **code** respositories are Jupyter Notebooks that users have created, analyzing the datasets of Kaggle. You could consult these notebooks for educational purposes.\n",
    "- You could explore **discussion** for any topics you think is relevant for your use cases.\n",
    "- Kaggle's **courses** could be helpful to gain foundational skills in e.g., Python, Machine Learnig, Pandas, Feature Engineering etc.\n",
    "\n",
    "We shall explore **datasets**, **code** and the Kaggle **courses**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Kaggle datasets\n",
    "\n",
    "In the _Datasets_ tab, you can view the different datasets submitted on the platform. For example, if you search for _test student_ in the search bar and look for the dataset _Predict Test Scores of students_, you can find an application in Education:\n",
    "\n",
    "<img src=\"../img/available_datasets/kaggle_datasets.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Description:\n",
    "\n",
    "It contains information about a test written by some students. It include features such as: School setting, School type, gender, pretetest scores among other. Explore the data to know more!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you click on it, you can find a link to download the datasets in _csv_ format:\n",
    "\n",
    "<img src=\"../img/available_datasets/kaggle_download_manual.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Kaggle code\n",
    "\n",
    "In the _Code_ tab, you can view the different source code (often in Jupyter Notebook format) submitted on the platform. For example, if you search for _classification_ in the search bar and look for the notebook _A Beginner's Approach to Classification_, you can find a machine learning tutorial:\n",
    "\n",
    "<img src=\"../img/available_datasets/kaggle_code.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The tutorial dives into a simple way to classify handwritten digits."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you click on it, you can find the contents of this tutorial:\n",
    "\n",
    "<img src=\"../img/available_datasets/kaggle_notebook.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Kaggle courses\n",
    "\n",
    "In the _Courses_ tab, you can view the free python courses developed by the platform. For example, if you look for  _Pandas_, you can find a tutorial on the Pandas package to work with tabular data in Python:\n",
    "\n",
    "<img src=\"../img/available_datasets/kaggle_courses.png\" width=\"800px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up AWS credentials on the machine.\n",
    "\n",
    "You can use the AWS SDK for Python (Boto3) to create, configure, and manage AWS services, such as Amazon Elastic Compute Cloud (Amazon EC2) and Amazon Simple Storage Service (Amazon S3). The SDK provides an object-oriented API as well as low-level access to AWS services. For more info about Boto3, you can consult the [Boto3 Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html).\n",
    "\n",
    "> **Remark**: You will need to install Boto3 package in the terminal in your machine using conda install.\n",
    "\n",
    "If you **run several commands on this tutorial**, it might be that you require AWS credentials. This can be done by using the *init_aws_credentials.py* that was provided to you at the kick-off meeting. Please contact the technical team if this was not provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Run the code script below to run the init_aws_credentials.py script to create an access key pair.\n",
    "%run init_aws_credentials.py\n",
    "\n",
    "## The script will prompt you to provide your BDTI credentials, like this:\n",
    "# Username: <your username>@bdti.cef.eu\n",
    "# Password: <your password>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we have to instruct the Boto3 SDK to use the SAML profile defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import boto3\n",
    "\n",
    "boto3.setup_default_session(profile_name='saml')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations! Now you are ready to connect to the BDTI cloud resources using Boto3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Downloading and uploading data in AWS S3 using Boto3\n",
    "\n",
    "The first step as a BDTI Pilot member is to upload your local or online data to the BDTI AWS S3 bucket, which is the object storage solution in BDTI for storing and sharing data. This way, other BDTI resources such as your Data Science Studio and your WorkSpace can use your data in the S3 bucket.\n",
    "\n",
    "To access your AWS S3 bucket, you need to first install the latest version of Boto3 and configure it (see previous section).\n",
    "\n",
    "We start by specifying the AWS S3 bucket service you want to connect to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import boto3\n",
    "\n",
    "# We want to connect to the S3 buckets.\n",
    "s3_resource = boto3.resource('s3')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the following codes allows you to upload a file to your S3 bucket and to download a file from your S3 bucket."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining bucket, filename to upload, and where location to upload\n",
    "bucket_name = \"cefbdti-miglierina-eu-west-1\"\n",
    "local_filename = '../example-data/color.json'\n",
    "s3_filename = 'example-data/color-uploaded.json'\n",
    "\n",
    "# Upload a new file\n",
    "data = open(local_filename, 'rb')\n",
    "s3_resource.Bucket(bucket_name).put_object(Key=s3_filename, Body=data)\n",
    "\n",
    "print(\"File uploaded in S3!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining bucket, filename to download from S3, and where location to download to locally\n",
    "bucket_name = \"cefbdti-miglierina-eu-west-1\"\n",
    "s3_filename = 'example-data/color-uploaded.json'\n",
    "local_filename = '../example-data/color-downloaded.json'\n",
    "\n",
    "# Download a file from S3\n",
    "s3_client = boto3.client('s3')\n",
    "s3_client.download_file(bucket_name, s3_filename, local_filename)\n",
    "\n",
    "print(\"File downloaded from S3!\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "metadata": {
   "interpreter": {
    "hash": "63fd5069d213b44bf678585dea6b12cceca9941eaf7f819626cde1f2670de90d"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

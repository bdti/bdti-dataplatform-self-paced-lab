{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img align=\"right\" src=\"../img/DEP.png\" width=\"400px\">\n",
    "\n",
    "# Onboarding Data Science Studio (with Python)\n",
    "\n",
    "Contributor: Wu, Ze Wen (https://github.com/zewenwu)\n",
    "\n",
    "This tutorial focuses on connecting to BDTI Cloud resources programmatically using Jupyter Notbook and Python in the BDTI Data Science Studio. \n",
    "\n",
    "Using the user interface from different BDTI Cloud resources can be timeconsuming. Code snippets from this Notebooks can be used when you want to create a single Jupyter Notebook that connects to multiple BDTI resources programmatically.\n",
    "\n",
    "In this tutorial, we introduce the different use cases of connecting to the BDTI Cloud resources programmatically using your Linux Data Science Studio.\n",
    "\n",
    "\n",
    "Contents:\n",
    "- Setting up Conda virtual environments in Jupyter Notebooks.\n",
    "- Downloading and uploading data in AWS S3 using Boto3.\n",
    "- Connect to your AWS MySQL server using MySQL connector module.\n",
    "- Connect to your AWS ElasticSearch using elasticsearch-py\n",
    "\n",
    "What you will learn:\n",
    "- Create and install Python packages in Conda virtual environments and import the environment in Jupyter Notebooks.\n",
    "- Basic commands on how you can download data from and upload data to your BDTI AWS S3 bucket.\n",
    "- Introduction to connecting to AWS MySQL server using Python.\n",
    "- Introduction to connecting to AWS ElasticSearch using Python.\n",
    "\n",
    "Remark:\n",
    "- You have to run the code cells in sequence. Some code blocks may depend on variables from previous code blocks.\n",
    "\n",
    "Source:\n",
    "- [Anaconda Documentation](https://docs.anaconda.com/)\n",
    "- [Anaconda Cheat Sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)\n",
    "- [w3schools](https://www.w3schools.com/python/python_mysql_create_db.asp)\n",
    "- [Pynative Python Programming](https://pynative.com/python-mysql-database-connection/)\n",
    "- [Boto3 Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conda virtual environments in Jupyter Notebooks\n",
    "\n",
    "When you start a new Python project, you will probably need to install several dependency packages that will be needed in your Python code. The best practice is to create a new Conda environment and install all the required Python packages into this Conda environment without interferring with the Conda environments of other users of the same project.\n",
    "\n",
    "Additionally, we provide instructions on how to list the Conda environment in Jupyter Notebooks.\n",
    "\n",
    "> **Remark**: All the code in this section should run in the Bash of your machine.\n",
    "\n",
    "### Viewing all the Conda environments\n",
    "\n",
    "You can view the existing Conda environments by using the **info** command of Conda:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda info --envs```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating a new Conda environment\n",
    "\n",
    "To create a new Conda environment to install all your dependency packages, you can use the **create** command of Conda. In the example below, we will create a new Conda environment named *my_env*, using Python 3.6. You can substitude the name to your liking."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda create --name my_env python=3.6```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Activate a Conda environment\n",
    "\n",
    "Now that you have created a new Conda environment, you need to activate it so that all the future Python packages will be installed in this new Conda environment. To do this, you need to use the **activate** command of Conda."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda activate my_env```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Furthermore, you can check that your Conda environment has been activated using the **info** command. You can confirm by observing that the start (_*_) is next to your listed Conda environment. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda info --envs```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setup Conda environment on Jupyter Notebooks\n",
    "\n",
    "In the new Conda environment, you need to set it up in Jupyter Notebooks to be able to refer it when you work in Jupyter Notebooks.\n",
    "\n",
    "First, install ipykernel using the following command to be executed in the Bash."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda install -c anaconda ipykernel```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, list your Conda environment as one of the available Conda environments in Jupyter Notebook using the following command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python -m ipykernel install --user --name=my_env```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, you can create a new Jupyter Notebooks using the Conda environment you have just added. Please check this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installing packages in your Conda environment\n",
    "\n",
    "**All the packages that your Jupyter Notebooks depend on should be installed in the Bash of the machine.** This can be done using the **install** command. For a list of all the available packages in Anaconda, consult the [its Anaconda Documentation](https://docs.anaconda.com/anaconda/packages/pkg-docs/). For example you can install the *matplotlib* package this way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda install matplotlib```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Listing packages in your Conda environment\n",
    "\n",
    "To list all the packages in your current Conda environment, you can use the **list** command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda list```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing packages in you Conda environment\n",
    "\n",
    "You could remove a list of package from a specific Conda environment. For example, you could remove the packages *matplotlib* and *scikit-learn* from the Conda environment *my_env* using the **remove** command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda remove --name my_env matplotlib scikit-learn```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deleting a Conda environment\n",
    "\n",
    "You can delete a Conda environment and all the packages in it using the **env remove** command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```conda env remove --name my_env```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have\n",
    "\n",
    "Other use cases of Conda are out of scope in this tutorial. \n",
    "For more info regarding Conda environments and Conda commands, please consult:\n",
    "- [Anaconda Documentation](https://docs.anaconda.com/)\n",
    "- [Anaconda Cheat Sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Downloading and uploading data in AWS S3 using Boto3\n",
    "\n",
    "The first step as a BDTI Pilot member is to upload your local or online data to the BDTI AWS S3 bucket, which is the object storage solution in BDTI for storing and sharing data. This way, other BDTI resources such as your Data Science Studio and your WorkSpace can use your data in the S3 bucket.\n",
    "\n",
    "To access your AWS S3 bucket, you need to first install the latest version of Boto3 and configure it (see previous section).\n",
    "\n",
    "We start by fetching the EC2 IAM role of the Data Science Studio. Then, we specify the AWS S3 bucket service you want to connect to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [],
   "source": [
    "import boto3\n",
    "from botocore.credentials import InstanceMetadataProvider, InstanceMetadataFetcher\n",
    "\n",
    "provider = InstanceMetadataProvider(iam_role_fetcher=InstanceMetadataFetcher(timeout=1000, num_attempts=2))\n",
    "creds = provider.load().get_frozen_credentials()\n",
    "s3_resource = boto3.resource('s3', aws_access_key_id=creds.access_key, aws_secret_access_key=creds.secret_key, aws_session_token=creds.token)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the following codes allows you to upload a file to your S3 bucket and to download a file from your S3 bucket."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "File uploaded in S3!\n"
     ]
    }
   ],
   "source": [
    "# Defining bucket, filename to upload, and where location to upload\n",
    "bucket_name = \"cefbdti-technicalworkshop-eu-west-1\"\n",
    "local_filename = '../example-data/color.json'\n",
    "s3_filename = 'example-data/color-uploaded.json'\n",
    "\n",
    "# Upload a new file\n",
    "data = open(local_filename, 'rb')\n",
    "s3_resource.Bucket(bucket_name).put_object(Key=s3_filename, Body=data)\n",
    "\n",
    "print(\"File uploaded in S3!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "File downloaded from S3!\n"
     ]
    }
   ],
   "source": [
    "# Defining bucket, filename to download from S3, and where location to download to locally\n",
    "bucket_name = \"cefbdti-technicalworkshop-eu-west-1\"\n",
    "s3_filename = 'example-data/color-uploaded.json'\n",
    "local_filename = '../example-data/color-downloaded.json'\n",
    "\n",
    "# Download a file from S3\n",
    "s3_resource.Bucket(bucket_name).download_file(s3_filename, local_filename)\n",
    "\n",
    "print(\"File downloaded from S3!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connect to your AWS MySQL server using MySQL connector module\n",
    "\n",
    "If your BDTI Pilot contains a MySQL Server, you can communicate with it using Python commands. To do this, you have to install the *MySQL connector module*.\n",
    "\n",
    "### Connect to your MySQL server\n",
    "You can use the **connector** method from this module to connect to your MySQL Database.\n",
    "\n",
    "First, you have to specify the hostname, master username, and its password. Please contact the technical team if this was not provided. Then, you can run the connector method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Please run the following command in bash inside your Conda environment.\n",
    "# pip install mysql-connector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Import the modules\n",
    "import mysql.connector\n",
    "from mysql.connector import Error"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Connected to MySQL server: Version  8.0.17\n"
     ]
    }
   ],
   "source": [
    "## Specify the connection information to your MySQL server.\n",
    "hostname=\"xxx.xxx.eu-west-1.rds.amazonaws.com\"\n",
    "username=\"databaseadmin\"\n",
    "pw=\"xxx\"\n",
    "\n",
    "## Try to establish a connection with your MySQL server.\n",
    "try:\n",
    "    connection = mysql.connector.connect(host=hostname, user=username, password=pw)\n",
    "    server_Info = connection.get_server_info()\n",
    "    print(\"Connected to MySQL server: Version \", server_Info)\n",
    "except Error as e:\n",
    "    print(\"Error while connecting to MySQL server\", e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a new MySQL database\n",
    "After you established an connection with the MySQL server, you can execute SQL operations using the **cursor** method.\n",
    "\n",
    "For example, you can create a new database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Error while creating a new database: 1007 (HY000): Can't create database 'Electronics'; database exists\n"
     ]
    }
   ],
   "source": [
    "## Specify the name of the new database.\n",
    "db_name = \"Electronics\"\n",
    "\n",
    "## Try to create a new database.\n",
    "mycursor = connection.cursor()\n",
    "try:\n",
    "    mycursor.execute(\"CREATE DATABASE \" + db_name)\n",
    "    print(\"Created a new database:  \", db_name)\n",
    "except Error as e:\n",
    "    print(\"Error while creating a new database:\", e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connect to an existing MySQL database\n",
    "\n",
    "Now you can establish an existing MySQL database by running the following code snippets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Import the modules\n",
    "import mysql.connector\n",
    "from mysql.connector import Error"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You are connected to database:  ('Electronics',)\n"
     ]
    }
   ],
   "source": [
    "## Specify the connection information to your MySQL database.\n",
    "hostname=\"xxx.xxx.eu-west-1.rds.amazonaws.com\"\n",
    "username=\"databaseadmin\"\n",
    "pw=\"xxx\"\n",
    "\n",
    "db_name = \"Electronics\"\n",
    "\n",
    "## Try to establish a connection with your MySQL database.\n",
    "try:\n",
    "    connection = mysql.connector.connect(host=hostname, database=db_name, user=username, password=pw)\n",
    "    mycursor = connection.cursor()\n",
    "    mycursor.execute(\"select database();\")\n",
    "    record = mycursor.fetchone()\n",
    "    print(\"You are connected to database: \", record)\n",
    "except Error as e:\n",
    "    print(\"Error while connecting to MySQL database\", e)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a MySQL table\n",
    "\n",
    "Now you can create a tabls in MySQL using Python. For example, we can create the table *Laptop* under the *Electronics* database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Import the modules\n",
    "import mysql.connector\n",
    "from mysql.connector import Error"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Failed to create table in MySQL: 1050 (42S01): Table 'Laptop' already exists\n"
     ]
    }
   ],
   "source": [
    "## Remark: We assume that you already established a MySQL database connection in the variable 'connection'.\n",
    "\n",
    "## Try to create a new table in a Database connection.\n",
    "try:\n",
    "    mySql_Create_Table_Query = \"\"\"CREATE TABLE Laptop ( \n",
    "                             Id int(11) NOT NULL,\n",
    "                             Name varchar(250) NOT NULL,\n",
    "                             Price float NOT NULL,\n",
    "                             Purchase_date Date NOT NULL,\n",
    "                             PRIMARY KEY (Id)) \"\"\"\n",
    "\n",
    "    mycursor = connection.cursor()\n",
    "    result = mycursor.execute(mySql_Create_Table_Query)\n",
    "    print(\"Laptop Table created successfully \")\n",
    "\n",
    "except mysql.connector.Error as error:\n",
    "    print(\"Failed to create table in MySQL: {}\".format(error))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Closing the MySQL connection\n",
    "\n",
    "You can close the cursor and the MySQL connection using the following code cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MySQL connection is now closed.\n"
     ]
    }
   ],
   "source": [
    "mycursor.close()\n",
    "connection.close()\n",
    "print(\"MySQL connection is now closed.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other use cases around MySQL connection using Python is out of scope in this tutorial.\n",
    "\n",
    "For more info around Python commands when connecting to your BDTI MySQL Database, please consult:\n",
    "- [w3schools](https://www.w3schools.com/python/python_mysql_create_db.asp)\n",
    "- [Pynative Python Programming](https://pynative.com/python-mysql-database-connection/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connect to your AWS ElasticSearch using elasticsearch-py\n",
    "\n",
    "If your BDTI Pilot contains an AWS ElasticSearch server, you can communicate with it using Python commands. To do this, you have to install the ElasticSearch connector module.\n",
    "\n",
    "### Connect to your AWS ElasticSearch server\n",
    "\n",
    "You can use the connector method from this module to connect to your ElasticSearch server.\n",
    "\n",
    "First, you have to specify the host endpoint. Please contact the technical team if this was not provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "## Import the modules\n",
    "import os\n",
    "import elasticsearch\n",
    "from elasticsearch import Elasticsearch\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Connection Established!\n"
     ]
    }
   ],
   "source": [
    "## Define the host endpoint.\n",
    "host = 'https://vpc-xxx-crel-xxx.eu-west-1.es.amazonaws.com'\n",
    "\n",
    "## Establish a connection with your ElasticSearch server.\n",
    "es = Elasticsearch(host)\n",
    "\n",
    "## Test connection\n",
    "if es.ping():\n",
    "    print('Connection Established!')\n",
    "else:\n",
    "    print('Connection NOT Established!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a new Index\n",
    "After you established an connection with the ElasticSearch server, you can create a new index using the **indices.create** method. Then, you can use the **indices.exists** method to check if the index exists.\n",
    "\n",
    "For example,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 58,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "## Create a new index\n",
    "es.indices.create(index='my-index')\n",
    "## Check if index exists\n",
    "es.indices.exists(index='my-index')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Delete an existing Index\n",
    "After you established an connection with the ElasticSearch server, you can delete an existing index using the **indices.delete** method. Then, you can use the **indices.exists** method to check if the index exists.\n",
    "\n",
    "For example,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 59,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "## Create a new index\n",
    "es.indices.delete(index='my-index')\n",
    "## Check if index exists\n",
    "es.indices.exists(index='my-index')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inserting sample documents\n",
    "\n",
    "Let's insert some sample data into the ElasticSearch server. You can use the **index** method to add documents at an index with a particula document type and id. Like this,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'_index': 'my_docs',\n",
       " '_type': 'school',\n",
       " '_id': '3',\n",
       " '_version': 7,\n",
       " 'result': 'updated',\n",
       " '_shards': {'total': 2, 'successful': 1, 'failed': 0},\n",
       " '_seq_no': 6,\n",
       " '_primary_term': 2}"
      ]
     },
     "execution_count": 60,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "doc_1 = {\"subject\": \"Math\", \"content\": \"1+1=2\"}\n",
    "doc_2 = {\"subject\": \"Essay\", \"contents\": \"To be or not to be.\"}\n",
    "doc_3 = {\"subject\": \"Letter\", \"header\": \"Dear Professor\"}\n",
    "\n",
    "es.index(index=\"my_docs\", doc_type=\"school\", id=1, body=doc_1)\n",
    "es.index(index=\"my_docs\", doc_type=\"school\", id=2, body=doc_2)\n",
    "es.index(index=\"my_docs\", doc_type=\"school\", id=3, body=doc_3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Getting sample documents\n",
    "\n",
    "You can get a document using the **get** method by specifying the index, doc_type and id. Like, this"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'subject': 'Math', 'content': '1+1=2'}"
      ]
     },
     "execution_count": 61,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "res = es.get(index=\"my_docs\", doc_type=\"school\", id=1)\n",
    "res[\"_source\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Searching for a document\n",
    "\n",
    "You can search for a document using query language using the **search** method by providing the index and the query body. Like this,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'total': {'value': 1, 'relation': 'eq'},\n",
       " 'max_score': 0.2876821,\n",
       " 'hits': [{'_index': 'my_docs',\n",
       "   '_type': 'school',\n",
       "   '_id': '3',\n",
       "   '_score': 0.2876821,\n",
       "   '_source': {'subject': 'Letter', 'header': 'Dear Professor'}}]}"
      ]
     },
     "execution_count": 62,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "body = {\n",
    "    \"from\":0,\n",
    "    \"size\":2,\n",
    "    \"query\": {\n",
    "        \"match\": {\n",
    "            \"header\":\"Professor\"\n",
    "        }\n",
    "    }\n",
    "}\n",
    "\n",
    "res = es.search(index=\"my_docs\", body=body)\n",
    "res['hits']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deleting a document\n",
    "\n",
    "You can delete a document using the **delete** method by providing the index and the id of the document. Like this,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {},
   "outputs": [],
   "source": [
    "res = es.delete(index=\"my_docs\", id=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other use cases for working with ElasticSearch using Python is out of scope in this tutorial. For more info, please consult:\n",
    "- [Full ElastocSearch Documentation](https://elasticsearch-py.readthedocs.io/en/v7.12.0/).\n",
    "- [Official Elastic Website](https://www.elastic.co/guide/en/elasticsearch/client/python-api/current/examples.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "metadata": {
   "interpreter": {
    "hash": "63fd5069d213b44bf678585dea6b12cceca9941eaf7f819626cde1f2670de90d"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

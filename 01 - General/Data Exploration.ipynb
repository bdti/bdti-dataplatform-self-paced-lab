{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img align=\"right\" src=\"../img/DEP.png\" width=\"400px\">\n",
    "\n",
    "# Data Exploration on Open EU COVID-19 data (with Python)\n",
    "\n",
    "Contributor: Wu, Ze Wen (https://github.com/zewenwu)\n",
    "\n",
    "In this notebook, we will explore some of the basic capabilities of Python's Pandas package for the data science's role to explore,\n",
    "analyze and visualize data. For numerical analysis of tabular data, the Pandas package includes specific data types and functions for working with two-dimensional tables of data in Python. The Pandas package offers a more convenient structure to work with data - the DataFrame.\n",
    "\n",
    "In this tutorial, we shall use the worldwide COVID-19 data up to December 2020, extracted from the [EU Open Data Portal](https://data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data). Every day between 6.00 and 10.00 CET, a team of epidemiologists screens up to 500 relevant sources to collect the latest figures. The data screening is followed by ECDC’s standard epidemic intelligence process for which every single data entry is validated and documented in an ECDC database.\n",
    "\n",
    "Contents:\n",
    "- Exploring tabular data with Pandas DataFrame\n",
    "- Visualize data with Matplotlib\n",
    "- Descriptive analysis\n",
    "- Statistical analysis\n",
    "- Appendix 1: The normal distribution\n",
    "- Appendix 2: Other file formats\n",
    "\n",
    "What you will learn:\n",
    "- Exploring tabular data with Pandas package using Python\n",
    "- Make customized data visualization with Matplotlib package using Python\n",
    "- Introduction on how to describe your data using Python\n",
    "- Introduction to statistical terminology and statistical visualization to describe your data\n",
    "\n",
    "Remark:\n",
    "- You have to run the code cells in sequence. Some code blocks may depend on variables from previous code blocks.\n",
    "- If you are executing this tutorial on a local computer, please setup the Anaconda3 Python environment before executing the following Python codes. In the Big Data Test Infrastructure (BDTI), this Python environment will be set up for you. Furthermore, you can utilize the BDTI resources to do computational heavy calculations and data storage.\n",
    "\n",
    "Source:\n",
    "- [Microsoft's ml-basics tutorials](https://github.com/MicrosoftDocs/ml-basics)\n",
    "- [Pandas documentation](https://pandas.pydata.org/docs/)\n",
    "- [Matplotlib documentation](https://matplotlib.org/3.3.2/contents.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploring tabular data with Pandas dataframes\n",
    "\n",
    "We can download the most recent COVID-19 data from the EU Open Data Portal in the local folder *eu-open-data*.\n",
    "\n",
    "The dataset is saved in the folder eu-open-data in *csv-format*. This is a common data format where the information is delimited using a symbol such as **,** or **;**. To import this data as a Pandas DataFrame into the memory of your computer, the **read_csv** method can be used from the Pandas package. In this method, you need to provide which **delimiter** that is used in the dataset and whether a **header** is present. The header can contain schema information on what the numbers of the data represents. \n",
    "\n",
    "More information on Pandas DataFrame can be found in the [Pandas documentation](https://pandas.pydata.org/docs/).\n",
    "\n",
    "Remark: \n",
    "- To import data from other file formats, please look into Appendix 2.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import requests\n",
    "\n",
    "# Download most recent data from EU Open Data Portal in a local file\n",
    "url = 'https://opendata.ecdc.europa.eu/covid19/casedistribution/csv'\n",
    "r = requests.get(url, allow_redirects=True)\n",
    "open('../eu-open-data/COVID-19-geographic-disbtribution-worldwide-online.csv', 'wb').write(r.content)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, we shall work with an older version of the data so that the data stays consistent with this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "# Import the data from local folder\n",
    "covid_data = pd.read_csv('../eu-open-data/COVID-19-geographic-disbtribution-worldwide-online-older.csv', delimiter=',', header='infer')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can visualize the first few rows of the tabular dataset by using the **head** method of the Pandas DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display some instances\n",
    "covid_data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we can visualize a few random rows of the tabular datasets by using the **sample** method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sample some instances\n",
    "covid_data.sample(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We observe that the dataset contains information such as:\n",
    "- The date of the logged COVID-19 cases (dateRep, day, month, year)\n",
    "- The number of COVID-19 cases (cases)\n",
    "- The number of COVID-19 deaths (deaths)\n",
    "- The country or territory of the COVID-19 numbers and identifiers (countriesAndTerritories, geoID, countryterritoryCode)\n",
    "- The population of the country or territory as of 2019 (popData2019)\n",
    "- The continent of the country or territory (continentExp)\n",
    "- The cumulative number for 14 days of COVID-19 cases (per 100 000).\n",
    "\n",
    "If you want to access a particular row and columns of this dataset, this can be realized by using the **loc** or the **iloc** method. The **loc** method references the assigned indices of the rows of the dataset, while the **iloc** method references the relative indices of the rows of the dataset (index 0 from the first row)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(covid_data.loc[0])\n",
    "print('\\n')\n",
    "print(covid_data.iloc[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The difference between these two methods is that **loc** locates based on the provided *row index*, while **iloc** locates based on the *relative index*. For example, **loc\\[0\\]** locates the index 0, which might be the first row, while **iloc\\[0\\]** always references the first row. \n",
    "\n",
    "Furthermore, you can use the **loc** method to filter by a given conditional statement. For example, filtering on the Belgian COVID-19 numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "covid_data.loc[covid_data[\"countriesAndTerritories\"] == \"Belgium\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, you can skip the **loc** method altogether to achieve the same result, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "covid_data[covid_data[\"countriesAndTerritories\"] == \"Belgium\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The common issue of dealing with data is dealing with incomplete or missing data. The **isnull** method of a Dataframe can be used to identify which values are null. This returns a boolean (True or False variable) table which indicates whether the value is null or not. To have a summary of how many null values each features contains, use the **sum** method, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "covid_data.isnull().sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What do we see here? We observe that some of the geoId, countryterritoryCode and continentExp are not defined. Also some of the Cumulative_number_for_14_days_of_COVID-19_cases_per100000 are not defined. We could fill in with information defined by ourselves. However, in this tutorial, we shall remove these features.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "covid_data = covid_data.dropna(axis=1, how='any')\n",
    "covid_data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's save the Belgium COVID-19 cases in a variable **covid_bel**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "covid_bel = covid_data.loc[covid_data[\"countriesAndTerritories\"] == \"Belgium\"]\n",
    "covid_bel.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize data with Matplotlib\n",
    "\n",
    "Matplotlib is a comprehensive library for creating static, animated, and interactive visualizations in Python. We shall demonstrate its capabilities on the COVID-19 data. More information on the Matplotlib package can be found in the [Matplotlib documentation](https://matplotlib.org/3.3.2/contents.html).\n",
    "\n",
    "First, you need to import matplotlib as an object **plt**. Using its **plot** method, you can specify the x-axis and its y-axis values. Finally, the plot can be displayed using the **show** method. Like this,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Ensure plots are displayed inline in the notebook\n",
    "%matplotlib inline\n",
    "\n",
    "from matplotlib import pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "# Create a bar plot of date vs COVID-19 cases\n",
    "plt.plot(covid_bel.dateRep, covid_bel.cases)\n",
    "\n",
    "# Display the plot\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We observe that the above plot needs some improvements. *Matplotlib* offers a wide range of customization that can be applied to your plot. The following code:\n",
    "- Specifies a bigger figure dimension\n",
    "- Adds a title to the chart (so we know what it represents)\n",
    "- Adds labels to the X and Y (so we know which axis shows which data)\n",
    "- Adds a grid (to make it easier to determine the values for the bars)\n",
    "- Rotates the X markers (so we can read them)\n",
    "- Fixes the convention where dates should be ascending on the x-axis\n",
    "- Displays every 10 date ticks on the x-axis instead of all (to make it more readable)\n",
    "\n",
    "There is no need to memorize codes for customizing a plot. \n",
    "The best method for drawing a visual pleasing plot is by looking up the right commands online and trail-and-error. For example, you can consult [stackoverflow](https://stackoverflow.com/), which is a huge developer community where people post their coding problems and bugs for the community to solve. Another option is to consult your favorite search engine."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Figure with specified figure size.\n",
    "fig = plt.figure(figsize=(10,5))\n",
    "\n",
    "# Create a plot of date vs COVID-19 cases (also fix ascending dates convention)\n",
    "plt.plot(covid_bel.dateRep.loc[::-1], covid_bel.cases.loc[::-1])\n",
    "\n",
    "x = np.arange(covid_bel.shape[0])\n",
    "every = 10\n",
    "\n",
    "# Customize the chart\n",
    "plt.title('COVID-19 cases Belgium')\n",
    "plt.xlabel('Date')\n",
    "plt.xticks(np.arange(min(x), max(x)+1, every))\n",
    "plt.ylabel('Cases')\n",
    "plt.grid(color='#95a5a6', linestyle='--', linewidth=2, axis='y', alpha=0.7)\n",
    "plt.xticks(rotation=45)\n",
    "\n",
    "# Display the plot\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that is way better! We observe that there are periodical wells of COVID-19 cases in Belgium. This is because the COVID-19 cases in the weekends are not registered in the same manner as during the weekdays.\n",
    "To plot a more representative plot, we can plot the week average of the COVID-19 cases in Belgium (this is also done in the Belgian news).\n",
    "This can be achieved by the following code cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Function for calculating the weekaverages\n",
    "def calc_weekly_averages(dates, cases):\n",
    "    DAYS_TO_AVERAGE_OVER = 7\n",
    "    # Calculate weekly averages\n",
    "    avgs = []\n",
    "    for i in range(DAYS_TO_AVERAGE_OVER, cases.size):\n",
    "        w = cases[i-DAYS_TO_AVERAGE_OVER:i]\n",
    "        avgs.append(w.mean())\n",
    "\n",
    "    # Update the dates that correspond to these weekly averages\n",
    "    dates = dates.iloc[DAYS_TO_AVERAGE_OVER::]\n",
    "\n",
    "    # Return the derived variables\n",
    "    return dates, avgs\n",
    "\n",
    "# Calculate the weekly averages for the Belgium COVID-19 cases.\n",
    "weekly_avg_bel_dates = covid_bel.dateRep.loc[::-1]\n",
    "weekly_avg_bel_cases = covid_bel.cases.loc[::-1]\n",
    "weekly_avg_bel_dates, weekly_avg_bel_cases = calc_weekly_averages(weekly_avg_bel_dates, weekly_avg_bel_cases)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above code cell defines a custom *Python function* that calculates the week average given the dates and the cases of a particular period.\n",
    "\n",
    "Let us now plot the weekly averages of the COVID-19 cases of Belgium, using the same custom plot defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Figure with specified figure size.\n",
    "fig = plt.figure(figsize=(10,5))\n",
    "\n",
    "# Create a plot of date vs COVID-19 cases\n",
    "plt.plot(weekly_avg_bel_dates, weekly_avg_bel_cases)\n",
    "\n",
    "x = np.arange(weekly_avg_bel_dates.shape[0])\n",
    "every = 10\n",
    "\n",
    "# Customize the chart\n",
    "plt.title('COVID-19 cases Belgium')\n",
    "plt.xlabel('Date')\n",
    "plt.xticks(np.arange(min(x), max(x)+1, every))\n",
    "plt.ylabel('Cases')\n",
    "plt.grid(color='#95a5a6', linestyle='--', linewidth=2, axis='y', alpha=0.7)\n",
    "plt.xticks(rotation=45)\n",
    "\n",
    "# Display the plot\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can apply the same procedure to the neighboring countries \"Netherlands\" and \"France\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat for Netherlands\n",
    "covid_neth = covid_data.loc[covid_data[\"countriesAndTerritories\"]== \"Netherlands\"]\n",
    "weekly_avg_neth_dates = covid_neth.dateRep.loc[::-1]\n",
    "weekly_avg_neth_cases = covid_neth.cases.loc[::-1]\n",
    "weekly_avg_neth_dates, weekly_avg_neth_cases = calc_weekly_averages(weekly_avg_neth_dates, weekly_avg_neth_cases)\n",
    "\n",
    "# Repeat for France\n",
    "covid_fra = covid_data.loc[covid_data[\"countriesAndTerritories\"] == \"France\"]\n",
    "weekly_avg_fra_dates = covid_fra.dateRep.loc[::-1]\n",
    "weekly_avg_fra_cases = covid_fra.cases.loc[::-1]\n",
    "weekly_avg_fra_dates, weekly_avg_fra_cases = calc_weekly_averages(weekly_avg_fra_dates, weekly_avg_fra_cases)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot these three countries together. This can be easily achieved by recycling the previous code cells."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Figure with specified figure size.\n",
    "fig = plt.figure(figsize=(10,5))\n",
    "\n",
    "# Create a plot of date vs COVID-19 cases\n",
    "plt.plot(weekly_avg_bel_dates, weekly_avg_bel_cases, label=\"Belgium\")\n",
    "plt.plot(weekly_avg_neth_dates, weekly_avg_neth_cases, label=\"Netherlands\") # Add the Netherlands cases\n",
    "plt.plot(weekly_avg_fra_dates, weekly_avg_fra_cases, label=\"France\") # Add the France cases\n",
    "\n",
    "x = np.arange(weekly_avg_bel_dates.shape[0])\n",
    "every = 10\n",
    "\n",
    "# Customize the chart\n",
    "plt.title('COVID-19 cases neighbors')\n",
    "plt.xlabel('Date')\n",
    "plt.xticks(np.arange(min(x), max(x)+1, every))\n",
    "plt.ylabel('Cases')\n",
    "plt.grid(color='#95a5a6', linestyle='--', linewidth=2, axis='y', alpha=0.7)\n",
    "plt.xticks(rotation=45)\n",
    "\n",
    "# Limit the y-axis and display the plot\n",
    "plt.ylim(0, 20000)\n",
    "plt.legend(loc=\"upper left\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Descriptive analysis\n",
    "\n",
    "Let us explore the methods that describe our data.\n",
    "\n",
    "We observe that in Belgium, Netherlands and France, their first COVID-19 wave started approx. at 1 March 2020 and terminated approx. on 25 May 2020. \n",
    "Let us select the data during the first COVID-19 wave. This can be achieved by:\n",
    "- Using the **index** method that returns the registered index that satisfies a given conditional statement (in this case when the date is equal to the given dates).\n",
    "- Using the **truncate** method that truncates the dataframe given indices returned by **index** method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "DATE_FROM_TRUNCATE = '01/03/2020'\n",
    "DATE_TO_TRUNCATE = '25/05/2020'\n",
    "\n",
    "covid_bel_fw = covid_bel.truncate(covid_bel.index[covid_bel[\"dateRep\"] == DATE_TO_TRUNCATE].tolist()[0],\n",
    "                                covid_bel.index[covid_bel[\"dateRep\"] == DATE_FROM_TRUNCATE].tolist()[0])\n",
    "\n",
    "covid_bel_fw"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now Let us apply the week average over this truncated data and create a new Dataframe using the columns with the outputs of our previous defined **calc_weekly_averages** function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate weekly averages as before, but for first wave.\n",
    "weekly_avg_bel_dates = covid_bel_fw.dateRep.loc[::-1]\n",
    "weekly_avg_bel_cases = covid_bel_fw.cases.loc[::-1]\n",
    "weekly_avg_bel_dates, weekly_avg_bel_cases = calc_weekly_averages(weekly_avg_bel_dates, weekly_avg_bel_cases)\n",
    "\n",
    "# Defining the columns as DataFrames\n",
    "c1 = pd.DataFrame(weekly_avg_bel_dates, columns = ['dateRep'])\n",
    "c2 = pd.DataFrame(weekly_avg_bel_cases, columns = ['cases'])\n",
    "\n",
    "# Resetting the assigned DataFrame indices previously assigned.\n",
    "c1.reset_index(drop=True, inplace=True)\n",
    "c2.reset_index(drop=True, inplace=True)\n",
    "\n",
    "# Glueing the columns in one DataFrame\n",
    "weekly_avg_bel_fw = pd.concat([c1, c2], axis=1)\n",
    "weekly_avg_bel_fw"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imagine that the National Health Organisation of Belgium will declare the days where the week average of the COVID-19 cases are above 800 as \"critical\". We can add a new column in the DataFrame to indicate the critical days.\n",
    "\n",
    "First, we'll create a Pandas Series containing the critical/not-critical indicator (True or False), and then we'll concatenate that series as a new column (axis 1) in the DataFrame. This can be done like this,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "CRITICAL_LIMIT = 800\n",
    "critical  = pd.Series(weekly_avg_bel_fw['cases'] >= CRITICAL_LIMIT)\n",
    "weekly_avg_bel_fw = pd.concat([weekly_avg_bel_fw, critical.rename(\"is_critical\")], axis=1)\n",
    "\n",
    "weekly_avg_bel_fw"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "DataFrames are designed for tabular data, and you can use them to perform many of the kinds of data analytics operation you can do in a relational database; such as grouping and aggregating tables of data.\n",
    "\n",
    "For example, using the **groupby** method, we can \n",
    "- Count the number of days that are \"critical\" \n",
    "- Find the mean of the covid cases during the critical and non-critical days.\n",
    "\n",
    "This can be done like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Critical days count\")\n",
    "print(weekly_avg_bel_fw.groupby(weekly_avg_bel_fw.is_critical).dateRep.count())\n",
    "\n",
    "print(\"\\n\")\n",
    "\n",
    "print(\"Mean COVID-19 cases\")\n",
    "print(weekly_avg_bel_fw.groupby(weekly_avg_bel_fw.is_critical)['cases'].mean())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From these numbers of the data, we can make some statements:\n",
    "- \"During the first COVID-19 wave of Belgium, 33 days were 'critical' while the remaining 46 days were 'not-critical'.\"\n",
    "- \"During the first COVID-19 wave of Belgium, the average number of confirmed COVID-19 cases was 1247 during the 'critical' days and 348 during the 'non-critical' days.\"\n",
    "\n",
    "Finally, let us plot the COVID-19 cases of Belgium during the first wave, highlighting the days that were \"critical\" and count the \"critical\" days in a piechart. We shall do this using the subplot method of **matplotlib**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.ticker as tk\n",
    "\n",
    "# Create a Figure with specified figure size.\n",
    "fig, ax = plt.subplots(1, 2, figsize=(24,10))\n",
    "\n",
    "# Create first plot of date vs COVID-19 cases (indicating the critical days)\n",
    "ax[0].plot(weekly_avg_bel_fw.dateRep, weekly_avg_bel_fw.cases, \n",
    "            label = \"Non-critical days\", color = \"blue\")\n",
    "ax[0].plot(weekly_avg_bel_fw.dateRep[weekly_avg_bel_fw.is_critical], weekly_avg_bel_fw.cases[weekly_avg_bel_fw.is_critical], \n",
    "            label = \"Critical days\", color = \"red\")\n",
    "\n",
    "# Customize the chart\n",
    "ax[0].grid(color='#95a5a6', linestyle='--', linewidth=2, axis='y', alpha=0.7)\n",
    "ax[0].set_xticklabels(weekly_avg_bel_fw.dateRep, rotation=90)\n",
    "ax[0].legend(loc=\"upper left\")\n",
    "ax[0].set_title('Cases per day')\n",
    "\n",
    "# Create a second plot with pie chart of is_critical counts\n",
    "is_critical_counts = weekly_avg_bel_fw['is_critical'].value_counts()\n",
    "ax[1].pie(is_critical_counts, labels=is_critical_counts)\n",
    "ax[1].set_title('Critical Days')\n",
    "ax[1].legend(is_critical_counts.keys().tolist())\n",
    "\n",
    "# Display the plot\n",
    "fig.suptitle('COVID-19 cases Belgium during first wave')\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Statistical analysis\n",
    "\n",
    "Now you have some idea of using Python to manipulate and visualize data. Let us start analyzing it.\n",
    "A lot of data science is rooted in statistics, so we'll explore some basic statistical concepts.\n",
    "\n",
    "### Descriptive statistics and data distribution\n",
    "When examining a variable (for example the COVID-19 cases in Belgium), data scientists are particularly interested in its distribution (in other words, how are all the different grade values spread across the sample). The starting point for this exploration is often to visualize the data as a histogram, and see how frequently each value for the variable occurs.\n",
    "\n",
    "A histogram can be easily plotted using the **hist** method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Figure\n",
    "fig = plt.figure(figsize=(10,4))\n",
    "\n",
    "# Plot a histogram\n",
    "plt.hist(weekly_avg_bel_fw.cases)\n",
    "\n",
    "# Add titles and labels\n",
    "plt.title('Data Distribution')\n",
    "plt.xlabel('Value')\n",
    "plt.ylabel('Frequency')\n",
    "\n",
    "# Show the figure\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Measures of central tendency\n",
    "To understand the distribution better, we can examine so-called measures of central tendency; which is a fancy way of describing statistics that represent the \"middle\" of the data. The goal of this is to try to find a \"typical\" value. Common ways to define the middle of the data include:\n",
    "\n",
    "- The mean: A simple average based on adding together all of the values in the sample set, and then dividing the total by the number of samples.\n",
    "- The median: The value in the middle of the range of all of the sample values.\n",
    "\n",
    "Let's calculate these values using the Pandas methods for the \"critical\" and \"non-critical\" days, and show them on the above histogram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define new variables for future use, one indicating the critical days and one for the non-critical days\n",
    "weekly_avg_bel_fw_crit = weekly_avg_bel_fw.cases[weekly_avg_bel_fw.is_critical]\n",
    "weekly_avg_bel_fw_noncrit = weekly_avg_bel_fw.cases[-weekly_avg_bel_fw.is_critical]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Get statistics\n",
    "# Get the minimum and maximum Covid-19 cases of the first wave.\n",
    "min_val = weekly_avg_bel_fw.cases.min()\n",
    "max_val = weekly_avg_bel_fw.cases.max()\n",
    "\n",
    "# Get the mean and median of \"critical\" days\n",
    "mean_crit = weekly_avg_bel_fw_crit.mean()\n",
    "med_crit = weekly_avg_bel_fw_crit.median()\n",
    "\n",
    "# Get the mean and median of \"non-critial\" days\n",
    "mean_noncrit = weekly_avg_bel_fw_noncrit.mean()\n",
    "med_noncrit = weekly_avg_bel_fw_noncrit.median()\n",
    "\n",
    "# Print the results\n",
    "print('Minimum:{:.2f}\\nMaximum:{:.2f}\\n \\n'.format(min_val, max_val))\n",
    "print('Critial days:\\nMean:{:.2f}\\nMedian:{:.2f}\\n \\n'.format(mean_crit, med_crit))\n",
    "print('Non-critical days\\nMean:{:.2f}\\nMedian:{:.2f}\\n'.format(mean_noncrit, med_noncrit))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Figure\n",
    "fig = plt.figure(figsize=(10,4))\n",
    "\n",
    "# Plot a histogram\n",
    "plt.hist(weekly_avg_bel_fw.cases)\n",
    "\n",
    "# Add lines for the statistics\n",
    "plt.axvline(x=mean_crit, color = 'cyan', linestyle='dashed', linewidth = 2)\n",
    "plt.axvline(x=med_crit, color = 'cyan', linestyle='dotted', linewidth = 2)\n",
    "plt.axvline(x=mean_noncrit, color = 'red', linestyle='dashed', linewidth = 2)\n",
    "plt.axvline(x=med_noncrit, color = 'red', linestyle='dotted', linewidth = 2)\n",
    "\n",
    "# Add titles and labels\n",
    "plt.title('Data Distribution')\n",
    "plt.xlabel('Value')\n",
    "plt.ylabel('Frequency')\n",
    "\n",
    "# Show the figure\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We observe that the mean and the median coincide for the COVID-19 cases of the \"non-critical\" days in Belgium. This is just a coincidance.\n",
    "\n",
    "Another way to visualize the distribution of a variable is to use a boxplot (sometimes called a box-and-whiskers plot). Let's create one for the \"critical\", and another for the \"non-critical\" days."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a Figure\n",
    "fig = plt.figure(figsize=(10,10))\n",
    "\n",
    "# Plot a histogram\n",
    "plt.boxplot(weekly_avg_bel_fw_crit)\n",
    "plt.boxplot(weekly_avg_bel_fw_noncrit)\n",
    "\n",
    "# Add titles and labels\n",
    "plt.title('Boxplots')\n",
    "\n",
    "# Show the figure\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boxplot shows the distribution of the COVID-19 values in a different format than the histogram. The box part of the plot shows where the inner two quartiles of the data reside. The whiskers extending from the box show the outer two quartiles. The line in the box indicates the median value.\n",
    "\n",
    "It's often useful to combine histograms and boxplots, with the box plot's orientation changed to align it with the histogram."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "\n",
    "# Create a figure for 2 subplots (2 rows, 1 column)\n",
    "fig, ax = plt.subplots(2, 1, figsize = (10,5))\n",
    "\n",
    "# Plot the histogram   \n",
    "ax[0].hist(weekly_avg_bel_fw.cases)\n",
    "ax[0].set_ylabel('Frequency')\n",
    "\n",
    "# Add lines for the mean, median, and mode\n",
    "ax[0].axvline(x=mean_crit, color = 'cyan', linestyle='dashed', linewidth = 2)\n",
    "ax[0].axvline(x=med_crit, color = 'cyan', linestyle='dotted', linewidth = 2)\n",
    "ax[0].axvline(x=mean_noncrit, color = 'red', linestyle='dashed', linewidth = 2)\n",
    "ax[0].axvline(x=med_noncrit, color = 'red', linestyle='dotted', linewidth = 2)\n",
    "\n",
    "# Plot the boxplot   \n",
    "ax[1].boxplot(weekly_avg_bel_fw_crit, vert=False)\n",
    "ax[1].boxplot(weekly_avg_bel_fw_noncrit, vert=False)\n",
    "ax[1].set_xlabel('Value')\n",
    "\n",
    "# Add a title to the Figure\n",
    "fig.suptitle('Data Distribution')\n",
    "\n",
    "# Show the figure\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now this is a beautiful plot!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Measures of variance\n",
    "There's another aspect of distributions we can examine: how much variability is there in the data?\n",
    "\n",
    "Typical statistics that measure variability in the data include:\n",
    "- Range: The difference between the maximum and minimum. There's no built-in function for this, but it's easy to calculate using the min and max functions.\n",
    "- Variance: The average of the squared difference from the mean. You can use the built-in **var** method to find this.\n",
    "- Standard Deviation: The square root of the variance. You can use the built-in **std** method to find this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "rng = weekly_avg_bel_fw_crit.max() - weekly_avg_bel_fw_crit.min()\n",
    "var = weekly_avg_bel_fw_crit.var()\n",
    "std = weekly_avg_bel_fw_crit.std()\n",
    "print('\\n Critical days:\\n - Range: {:.2f}\\n - Variance: {:.2f}\\n - Std.Dev: {:.2f}'.format(rng, var, std))\n",
    "\n",
    "rng = weekly_avg_bel_fw_noncrit.max() - weekly_avg_bel_fw_noncrit.min()\n",
    "var = weekly_avg_bel_fw_noncrit.var()\n",
    "std = weekly_avg_bel_fw_noncrit.std()\n",
    "print('\\n Non-critical days:\\n - Range: {:.2f}\\n - Variance: {:.2f}\\n - Std.Dev: {:.2f}'.format(rng, var, std))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of these statistics, the standard deviation is generally the most useful. It provides a measure of variance in the data on the same scale as the data itself. The higher the standard deviation, the more variance there is when comparing values in the distribution to the distribution mean - in other words, the data is more spread out.\n",
    "\n",
    "The descriptive statistics we've used are the basis of statistical analysis; and because they're such an important part of exploring your data, there's a built-in **describe** method of the DataFrame object that returns the main descriptive statistics for all numeric columns. For example,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "weekly_avg_bel_fw_crit.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Appendix 1: The normal distribution\n",
    "\n",
    "When working with a normal distribution (also called Gaussian distribution), the standard deviation works with the particular characteristics of a normal distribution to provide even greater insight. Run the cell below to see the relationship between standard deviations and a variable assumed following the normal distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import scipy.stats as stats\n",
    "import math\n",
    "\n",
    "# Create a Figure\n",
    "fig = plt.figure(figsize=(10,8))\n",
    "\n",
    "# Plot the density\n",
    "mu = 0\n",
    "variance = 1\n",
    "s = math.sqrt(variance)\n",
    "x = np.linspace(mu - 4*s, mu + 4*s, 100)\n",
    "plt.plot(x, stats.norm.pdf(x, mu, s))\n",
    "\n",
    "# Annotate 1 stdev\n",
    "x1 = [mu-s, mu+s]\n",
    "y1 = stats.norm.pdf(x1, mu, s)\n",
    "plt.plot(x1,y1, color='magenta')\n",
    "plt.annotate('1 std (68.26%)', (x1[1],y1[1]))\n",
    "\n",
    "# Annotate 2 stdevs\n",
    "x2 = [mu-(s*2), mu+(s*2)]\n",
    "y2 = stats.norm.pdf(x2, mu, s)\n",
    "plt.plot(x2,y2, color='green')\n",
    "plt.annotate('2 std (95.45%)', (x2[1],y2[1]))\n",
    "\n",
    "# Annotate 3 stdevs\n",
    "x3 = [mu-(s*3), mu+(s*3)]\n",
    "y3 = stats.norm.pdf(x3, mu, s)\n",
    "plt.plot(x3,y3, color='orange')\n",
    "plt.annotate('3 std (99.73%)', (x3[1],y3[1]))\n",
    "\n",
    "# Show the location of the mean\n",
    "plt.axvline(mu, color='cyan', linestyle='dashed', linewidth=1)\n",
    "\n",
    "# plt.axis('off')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The horizontal lines show the percentage of data within 1, 2, and 3 standard deviations of the mean (plus or minus).\n",
    "\n",
    "In any normal distribution:\n",
    "- Approximately 68.26% of values fall within one standard deviation from the mean.\n",
    "- Approximately 95.45% of values fall within two standard deviations from the mean.\n",
    "- Approximately 99.73% of values fall within three standard deviations from the mean."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Appendix 2: Other file formats\n",
    "\n",
    "In the above tutorial, only csv-files are considered. However, the Pandas DataFrame can also import other file formats. In this appendix, we shall give a short description on how to import other file formats, such as *JSON-format* and *XML-format*.\n",
    "\n",
    "Let us import some example data,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "### Method for importing JSON-files.\n",
    "\n",
    "# Use the built-in method for reading JSON-files\n",
    "color_data = pd.read_json('../example-data/color.json')\n",
    "color_data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import xml.etree.ElementTree as et\n",
    "\n",
    "### Method for importing XML-files.\n",
    "\n",
    "# Define an XML parser for reading XML-files\n",
    "def parse_XML(xml_file, df_cols): \n",
    "    \"\"\"Parse the input XML file and store the result in a pandas \n",
    "    DataFrame with the given columns. \n",
    "    \n",
    "    The first element of df_cols is supposed to be the identifier \n",
    "    variable, which is an attribute of each node element in the \n",
    "    XML data; other features will be parsed from the text content \n",
    "    of each sub-element. \n",
    "    \"\"\"\n",
    "    \n",
    "    xtree = et.parse(xml_file)\n",
    "    xroot = xtree.getroot()\n",
    "    rows = []\n",
    "    \n",
    "    for node in xroot: \n",
    "        res = []\n",
    "        res.append(node.attrib.get(df_cols[0]))\n",
    "        for el in df_cols[1:]: \n",
    "            if node is not None and node.find(el) is not None:\n",
    "                res.append(node.find(el).text)\n",
    "            else: \n",
    "                res.append(None)\n",
    "        rows.append({df_cols[i]: res[i] \n",
    "                     for i, _ in enumerate(df_cols)})\n",
    "    \n",
    "    out_df = pd.DataFrame(rows, columns=df_cols)\n",
    "        \n",
    "    return out_df\n",
    "\n",
    "# Use the defined function.\n",
    "student_data = parse_XML(\"../example-data/student.xml\", [\"name\", \"email\", \"grade\", \"age\"])\n",
    "student_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, the downside to this last approach is that you need to know the structure of the XML file in advance. Source: [From XML to Pandas dataframes](https://medium.com/@robertopreste/from-xml-to-pandas-dataframes-9292980b1c1c)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "spl",
   "language": "python",
   "name": "spl"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
